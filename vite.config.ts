import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vitest/config';
import wasmPack from 'vite-plugin-wasm-pack';

export default defineConfig({
	plugins: [sveltekit(), wasmPack([],['client_profiler'])],
	server: {
		https: false,
		port: 5175,
		strictPort: true,
		open: true,
		cors: true,
		host: '0.0.0.0',
	},
	preview: {
		port: 4175,
		strictPort: false,
	},
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	}
});
