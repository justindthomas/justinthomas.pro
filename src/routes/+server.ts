import { sql } from '@vercel/postgres';
import { json } from '@sveltejs/kit';
import { env } from "$env/dynamic/private";
import type { RequestHandler } from '@sveltejs/kit'
import * as wasm from 'server_profiler';

function dateOk(date: number): boolean {
    const now: number = Date.now();
    console.log(now);
    return Math.abs(now - date) < 30000
}

export const POST: RequestHandler = async ({ request }) => {
    const { topic, email, phone, detail, user_agent, hash, date } = await request.json();

    const nonceSecret = env.NONCE_SECRET;

    const nonces: string[] = JSON.parse(wasm.get_nonces(nonceSecret));

    console.info(user_agent);
    console.info(date);

    if (dateOk(date)) {
        for (const nonce of nonces) {
            const data = `${user_agent}|${date}|${nonce}`;

            if (wasm.verify(hash, data)) {
                console.info(`VERIFIED NONCE ${nonce}`);
                await sql`INSERT INTO contact_requests (email, phone, topic, detail) VALUES ( ${email}, ${phone}, ${topic}, ${detail})`;
                break;
            } else {
                console.warn(`FAILED NONCE ${nonce}`);
            }
        }
    }

    return json({});
}