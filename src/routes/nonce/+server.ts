import { json } from '@sveltejs/kit';
import { env } from "$env/dynamic/private";
import type { RequestHandler } from '@sveltejs/kit'
import * as wasm from 'server_profiler';

export const GET: RequestHandler = async () => {
    const nonceSecret = env.NONCE_SECRET;

    const nonces: string[] = JSON.parse(wasm.get_nonces(nonceSecret));

    return json({ "nonce": nonces[0] });
}