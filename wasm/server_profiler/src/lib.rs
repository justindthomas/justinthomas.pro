use chrono::Utc;
use wasm_bindgen::prelude::*;

use argon2::{
    password_hash::{PasswordHash, PasswordVerifier},
    Argon2,
};

#[wasm_bindgen]
extern "C" {
    pub fn alert(s: &str);
}

#[wasm_bindgen]
pub fn get_uuid() -> String {
    uuid::Uuid::new_v4().to_string()
}

#[wasm_bindgen]
pub fn get_nonces(secret: String) -> String {
    let segment = Utc::now().timestamp() / 10;
    let current = blake3::hash(format!("{segment}{secret}").as_bytes());
    let previous = blake3::hash(format!("{}{secret}", segment - 1).as_bytes());

    serde_json::to_string(&vec![format!("{current}"), format!("{previous}")]).unwrap()
}

#[wasm_bindgen]
pub fn verify(hash: String, data: String) -> bool {
    let argon = Argon2::default();

    PasswordHash::new(&hash).ok().map_or(false, |hash| {
        argon.verify_password(data.as_bytes(), &hash).is_ok()
    })
}
