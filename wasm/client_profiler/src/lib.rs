use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};

use argon2::{
    password_hash::{ PasswordHasher, SaltString}, Argon2
};

#[wasm_bindgen]
extern {
    pub fn alert(s: &str);
}

#[derive(Serialize, Deserialize)]
pub struct UserAgent {
    pub user_agent: String,
    pub hash: String,
    pub date: String,
}

#[wasm_bindgen]
pub async fn get_ua(nonce: String) -> String {
    let window = web_sys::window().unwrap();
    let date = js_sys::Date::now().to_string();
    let navigator = window.navigator();
    let user_agent = navigator.user_agent().unwrap();

    let data = format!("{user_agent}|{date}|{nonce}");

    let mut rng = rand::thread_rng();
    let salt = SaltString::generate(&mut rng);
    let argon = Argon2::default();
    
    let hash = argon.hash_password(&data.into_bytes(), &salt).unwrap().to_string();

    serde_json::to_string(&UserAgent {
        user_agent,
        date,
        hash
    }).unwrap()
}
