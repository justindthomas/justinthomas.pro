# Installation

1. Have a working [Rust environment](https://www.rust-lang.org/tools/install).
2. Install `wasm-pack` with `cargo install wasm-pack`.
3. Clone this repository, and build with `web` target.

```
cd client_profiler
wasm-pack build --target web
```
